package com.unrc.sudoku.models;

import static org.junit.Assert.*;

import org.junit.*;
import com.unrc.sudoku.models.Game;

public class GameTest {
  private Game game;

  @Before
  public void setUp() {
    this.game = new Game();
  }

  @Test
  public void winGame() {
    System.out.println("\n <INITIAL GAME> \n");
    game.print();
    // row 1
    game.fill(4, 0, 0);
    game.fill(3, 0, 1);
    game.fill(5, 0, 2);
    game.fill(9, 0, 5);
    game.fill(8, 0, 7);
    // row 2
    game.fill(2, 1, 2);
    game.fill(5, 1, 3);
    game.fill(1, 1, 5);
    game.fill(4, 1, 6);
    game.fill(3, 1, 8);
    // row 3
    game.fill(7, 2, 2);
    game.fill(8, 2, 3);
    game.fill(3, 2, 4);
    game.fill(6, 2, 7);
    game.fill(2, 2, 8);
    // row 4
    game.fill(6, 3, 2);
    game.fill(9, 3, 4);
    game.fill(5, 3, 5);
    game.fill(3, 3, 6);
    game.fill(7, 3, 8);
    // row 5
    game.fill(3, 4, 0);
    game.fill(7, 4, 1);
    game.fill(8, 4, 4);
    game.fill(1, 4, 7);
    game.fill(5, 4, 8);
    // row 6
    game.fill(9, 5, 0);
    game.fill(1, 5, 2);
    game.fill(7, 5, 3);
    game.fill(4, 5, 4);
    game.fill(6, 5, 6);
    // row 7
    game.fill(5, 6, 0);
    game.fill(1, 6, 1);
    game.fill(2, 6, 4);
    game.fill(6, 6, 5);
    game.fill(8, 6, 6);
    // row 8
    game.fill(2, 7, 0);
    game.fill(8, 7, 2);
    game.fill(9, 7, 3);
    game.fill(7, 7, 5);
    game.fill(1, 7, 6);
    // row 9
    game.fill(6, 8, 1);
    game.fill(4, 8, 3);
    game.fill(2, 8, 6);
    game.fill(5, 8, 7);
    game.fill(9, 8, 8);

    System.out.println("\n <-------> \n");
    game.print();
    assertTrue(game.check());
  }
}
