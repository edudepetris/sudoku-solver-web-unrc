package com.unrc.sudoku.models;

import static org.junit.Assert.*;

import org.junit.*;
import com.unrc.sudoku.models.BoardGenerator;
import com.unrc.sudoku.models.Cell;
import com.unrc.sudoku.models.FakeBoardGenerator;
import com.unrc.sudoku.models.Grid;

public class GridTest {
  private Grid grid;

  @Before
  public void setUp() {
    BoardGenerator boardGenerator = new FakeBoardGenerator();
    this.grid = new Grid(boardGenerator);
  }

  @Test
  public void successFill() {
    assertTrue(grid.fill(4, 0, 0));
    assertSame(4, grid.getValue(0, 0));
  }

  @Test
  public void unsuccessFillByCell() {
    assertFalse(grid.fill(4, 1, 1));
    assertSame(8, grid.getValue(1, 1));
  }

  @Test
  public void unsuccessFillByPosition() {
    assertFalse(grid.fill(4, 100, 200));
  }

  @Test
  public void getValueOutOfRange() {
    assertSame(-1, grid.getValue(100, 200));
  }

  @Test
  public void sucesscClearCell() {
    grid.fill(4, 0, 0);
    assertTrue(grid.clear(0, 0));
    assertSame(0, grid.getValue(0, 0));
  }

  @Test
  public void unsucesscClearCellByCell() {
    assertFalse(grid.clear(1, 1));
  }

  @Test
  public void unsucesscClearCellByRange() {
    assertFalse(grid.clear(100, 200));
  }

  @Test
  public void getRow() {
    int row = 0;
    int[] expected = { 0, 0, 0, 2, 6, 0, 7, 0, 1 };
    assertArrayEquals(expected, grid.getRow(row));
  }

  @Test
  public void getColumn() {
    int column = 0;
    int[] expected = { 0, 6, 1, 8, 0, 0, 0, 0, 7 };
    assertArrayEquals(expected, grid.getColumn(column));
  }

  @Test
  public void getBlock() {
    int block = 5;
    int [][] expected = {
      {1, 0, 0},
      {6, 0, 2},
      {0, 0, 3}
    };
    assertArrayEquals(expected, grid.getBlock(block));
  }
}
