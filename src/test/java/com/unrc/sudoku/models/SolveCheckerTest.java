package com.unrc.sudoku.models;

import static org.junit.Assert.*;

import org.junit.*;
import com.unrc.sudoku.models.BoardGenerator;
import com.unrc.sudoku.models.FakeBoardGenerator;
import com.unrc.sudoku.models.Grid;
import com.unrc.sudoku.models.SolveChecker;

public class SolveCheckerTest {
  private SolveChecker checker;
  private Grid grid;

  @Before
  public void setUp() {
    BoardGenerator boardGenerator = new FakeBoardGenerator();
    this.grid = new Grid(boardGenerator);
    this.checker = new SolveChecker(grid);
  }

  @Test
  public void successRowValidForValue() {
    grid.fill(4, 0, 0);
    assertTrue(checker.rowValid(4, 0));
  }

  @Test
  public void unsuccessRowValidForValue() {
    grid.fill(2, 0, 0);
    assertFalse(checker.rowValid(2, 0));
  }

  @Test
  public void successColumnValidForValue() {
    grid.fill(4, 0, 0);
    assertTrue(checker.columnValid(4, 0));
  }

  @Test
  public void unsuccessColumnValidForValue() {
    grid.fill(6, 0, 0);
    assertFalse(checker.columnValid(6, 0));
  }

  @Test
  public void successBlockValidForValue() {
    grid.fill(5, 8, 8);
    assertTrue(checker.blockValid(5, 8, 8));
  }

  @Test
  public void unsuccessBlockValidForValue() {
    grid.fill(6, 8, 8);
    assertFalse(checker.blockValid(6, 8, 8));
  }
}