package com.unrc.sudoku.models;

import static org.junit.Assert.*;

import org.junit.*;
import com.unrc.sudoku.models.Cell;
import com.unrc.sudoku.models.SimpleBoardGenerator;

public class SimpleBoardGeneratorTest {

  @Test
  public void run() {
    int difficulty = 0;
    Cell[][] board = new SimpleBoardGenerator(difficulty).run();
    assertTrue(isBoardValid(board));
  }

  // TODO
  private boolean isBoardValid(Cell[][] board) {
    return true;
  }
}
