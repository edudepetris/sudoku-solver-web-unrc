package com.unrc.sudoku.models;

import static org.junit.Assert.*;

import org.junit.*;
import com.unrc.sudoku.models.BoardGenerator;
import com.unrc.sudoku.models.Cell;
import com.unrc.sudoku.models.FakeBoardGenerator;
import com.unrc.sudoku.models.Grid;
import com.unrc.sudoku.models.Solver;

public class SolverTest {
  private Grid grid;
  private Solver solver;

  @Before
  public void setUp() {
    BoardGenerator boardGenerator = new FakeBoardGenerator();
    this.grid = new Grid(boardGenerator);
    this.solver = new Solver(grid);
  }

  @Test
  public void sucessRowCheck() {
    // A { 4, 3, 5, 2, 6, 9, 7, 8, 1 }
    grid.fill(4, 0, 0);
    assertTrue(solver.check(0, 0));
    grid.fill(3, 0, 1);
    assertTrue(solver.check(0, 1));
    grid.fill(5, 0, 2);
    assertTrue(solver.check(0, 2));
    grid.fill(9, 0, 5);
    assertTrue(solver.check(0, 5));
    grid.fill(8, 0, 7);
    assertTrue(solver.check(0, 7));
  }

  @Test
  public void unsucessRowCheck() {
    // A { 4, 3, 5, 2, 6, 9, 7, 8, 1 }
    grid.fill(4, 0, 0);
    assertTrue(solver.check(0, 0));
    grid.fill(4, 0, 1);
    assertFalse(solver.check(0, 1));
  }

  @Test
  public void sucessColumnCheck() {
    // 0 { 4, 6, 1, 8, 3, 9, 5, 2, 7 }
    grid.fill(4, 0, 0);
    assertTrue(solver.check(0, 0));
    grid.fill(3, 4, 0);
    assertTrue(solver.check(4, 0));
    grid.fill(9, 5, 0);
    assertTrue(solver.check(5, 0));
    grid.fill(5, 6, 0);
    assertTrue(solver.check(6, 0));
    grid.fill(2, 7, 0);
    assertTrue(solver.check(7, 0));
  }

  @Test
  public void unsucessColumnCheck() {
    // 0 { 4, 6, 1, 8, 0, 0, 0, 0, 7 }
    grid.fill(4, 0, 0);
    assertTrue(solver.check(0, 0));
    grid.fill(4, 4, 0);
    assertFalse(solver.check(4, 0));
  }

  @Test
  public void sucessBlockCheck() {
    //     3  4  5        3  4  5
    // D { 1, 0, 0 }    { 1, 9, 5 }
    // E { 6, 0, 2 } => { 6, 8, 2 }
    // F { 0, 0, 3 }    { 7, 4, 3 }
    grid.fill(9, 3, 4);
    assertTrue(solver.check(4, 3));
    grid.fill(5, 3, 5);
    assertTrue(solver.check(3, 5));
    grid.fill(8, 4, 4);
    assertTrue(solver.check(4, 4));
    grid.fill(7, 5, 3);
    assertTrue(solver.check(5, 3));
    grid.fill(4, 5, 4);
    assertTrue(solver.check(5, 4));
  }

  @Test
  public void unsucessBlockCheck() {
    //     3  4  5        3  4  5
    // D { 1, 0, 0 }    { 1, 9, 5 }
    // E { 6, 0, 2 } => { 6, 8, 2 }
    // F { 0, 0, 3 }    { 7, 4, 3 }
    grid.fill(9, 3, 4);
    assertTrue(solver.check(3, 4));
    grid.fill(9, 5, 3);
    assertFalse(solver.check(3, 5));
  }
}
