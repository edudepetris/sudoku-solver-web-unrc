package com.unrc.sudoku.models;

import static org.junit.Assert.*;

import org.junit.*;
import com.unrc.sudoku.models.Cell;

public class CellTest {

  @Test
  public void createCellWithoutValue() {
    Cell cell = new Cell();
    assertSame(0, cell.getValue());
    assertTrue(cell.isModifiable());
  }

  @Test
  public void createCellWithValue() {
    Cell cell = new Cell(5);
    assertSame(5, cell.getValue());
    assertFalse(cell.isModifiable());
  }

  @Test
  public void setValue() {
    Cell cell = new Cell();
    cell.setValue(5);
    assertSame(5, cell.getValue());
  }

  @Test(expected = IllegalStateException.class)
  public void unsuccesSetValueByModifiable() {
    Cell cell = new Cell(5);
    cell.setValue(4);
  }

  @Test(expected = IllegalArgumentException.class)
  public void unsuccesSetValueByValue() {
    Cell cell = new Cell();
    cell.setValue(199);
  }

  @Test(expected = IllegalStateException.class)
  public void unsuccesCelarByModifiable() {
    Cell cell = new Cell(5);
    cell.clear();
  }

  @Test
  public void succesCelar() {
    Cell cell = new Cell();
    cell.setValue(4);
    cell.clear();
    assertSame(0, cell.getValue());
  }
}
