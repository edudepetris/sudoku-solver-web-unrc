package com.unrc.sudoku.controllers;

import static spark.Spark.*;
import static com.unrc.sudoku.util.RequestUtil.*;
import static com.unrc.sudoku.util.JsonUtil.*;

import com.fasterxml.jackson.databind.*;
import org.json.JSONObject;

import spark.Request;
import spark.Response;
import spark.Route;

// sudoku models
import com.unrc.sudoku.models.GridModel;
import com.unrc.sudoku.models.Grid;
import com.unrc.sudoku.models.Solver;

public class SudokuController {

  public static Route fecthNewSudoku = (Request request, Response response) -> {
    if (clientAcceptsJson(request)) {
      GridModel grid = new Grid();
      return dataToJson(grid.getGrid());
    }
    return "Default";
  };

  public static Route checkSudoku = (Request request, Response response) -> {
    String body = request.body();
    JsonNode arrNode = new ObjectMapper().readTree(body).get("board");
    int[] board = new int[arrNode.size()];
    for (int i = 0; i < arrNode.size(); i++) {
      board[i] = arrNode.get(i).asInt();
    }
    GridModel grid = new Grid(board);
    Solver solver = new Solver((Grid)grid);
    // build response
    JSONObject obj = new JSONObject();
    obj.put("valid", solver.check());
    return obj.toString();
  };
}