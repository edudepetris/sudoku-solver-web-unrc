package com.unrc.sudoku.models;

import com.unrc.sudoku.models.Cell;

public interface BoardGenerator {
  public Cell[][] run();
}