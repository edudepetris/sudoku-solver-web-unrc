package com.unrc.sudoku.models;

public class Solver {
  private Grid grid;
  private SolveChecker checker;

  public Solver(Grid grid) {
    this.grid = grid;
    this.checker = new SolveChecker(grid);
  }

  /**
   * @param row this is the number of row on sudoku(1-9).
   * @param column this is the number of column on sudoku(1-9)
   * @return true if board in row and column is valid or false if not.
   */
  public boolean check(int row, int column) {
    int value = grid.getValue(row, column);
    return checker.rowValid(value, row)
           && checker.columnValid(value, column)
           && checker.blockValid(value, row, column);
  }

  /**
   * @return true if board is valid or false if not.
   */
  public boolean check() {
    boolean solved = true;
    int value = 0;

    // FIXME: create a grid iterator for navigate the board.
    for (int row = 0; row < grid.rowSize() && solved; row++) {
      for (int column = 0; column < grid.columnSize() && solved; column++) {
        value = grid.getValue(row, column);
        if (value == 0) {
          continue;
        }
        solved = check(row, column);
      }
    }
    return solved;
  }
}
