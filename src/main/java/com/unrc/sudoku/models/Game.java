package com.unrc.sudoku.models;

import com.unrc.sudoku.models.Grid;
import com.unrc.sudoku.models.Solver;

public class Game {
  private Grid grid;
  private Solver solver;

  public Game() {
    this.grid = new Grid();
    this.solver = new Solver(grid);
  }

  public void fill(int value, int row, int column) {
    grid.fill(value, row, column);
  }

  public boolean check(int row, int column) {
    return solver.check(row, column);
  }

  public boolean check() {
    return solver.check();
  }

  public void print() {
    System.out.println("\n Board \n");
    System.out.println(grid.toString());
  }
}