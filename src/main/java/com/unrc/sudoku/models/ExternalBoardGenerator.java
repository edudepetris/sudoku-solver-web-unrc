package com.unrc.sudoku.models;

public class ExternalBoardGenerator implements BoardGenerator {
  static final int SIZE = 9;
  static final int EMPTY = 0;
  private int[][] values = new int[SIZE][SIZE];

  public ExternalBoardGenerator(int[] initialBoard) {
    int position = 0;
    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
        this.values[i][j] = initialBoard[position];
        position++;
      }
    }
  }

  /**
   * @return Cell[][] is an sudoku board valid to play.
  */
  public Cell[][] run() {
    Cell[][] board = new Cell[SIZE][SIZE];
    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
        int value = this.values[i][j];
        board[i][j] = value == EMPTY ? new Cell() : new Cell(value);
      }
    }
    return board;
  }
}