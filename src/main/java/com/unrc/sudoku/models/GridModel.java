package com.unrc.sudoku.models;

/**
 *  Interface for web sudoku.
 */
public interface GridModel {

  /**
   * @return int[] of board representation.
   */
  public int[] getGrid();
}