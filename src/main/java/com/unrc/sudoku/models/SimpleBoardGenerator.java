package com.unrc.sudoku.models;

public class SimpleBoardGenerator implements BoardGenerator {
  private int difficulty = 0;
  static final int SIZE = 9;
  static final int EMPTY = 0;

  public SimpleBoardGenerator(int difficulty) {
    this.difficulty = difficulty;
  }

  /**
   * @return Cell[][] is an sudoku board valid to play.
  */
  public Cell[][] run() {
    Cell[][] board = new Cell[SIZE][SIZE];
    int[][] values = {
      { 0, 0, 0, 2, 6, 0, 7, 0, 1 },
      { 6, 8, 0, 0, 7, 0, 0, 9, 0 },
      { 1, 9, 0, 0, 0, 4, 5, 0, 0 },
      { 8, 2, 0, 1, 0, 0, 0, 4, 0 },
      { 0, 0, 4, 6, 0, 2, 9, 0, 0 },
      { 0, 5, 0, 0, 0, 3, 0, 2, 8 },
      { 0, 0, 9, 3, 0, 0, 0, 7, 4 },
      { 0, 4, 0, 0, 5, 0, 0, 3, 6 },
      { 7, 0, 3, 0, 1, 8, 0, 0, 0 }
    };
    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
        int value = values[i][j];
        board[i][j] = value == EMPTY ? new Cell() : new Cell(value);
      }
    }
    return board;
  }
}