package com.unrc.sudoku.models;

import java.util.*;

public class Grid implements GridModel {

  static final int OUT_OF_RANGE = -1;
  static final int SIZE = 9;
  static final int BLOCK_N = 3;
  private Cell[][] board;

  public Grid(BoardGenerator generator) {
    this.board = generator.run();
  }

  public Grid() {
    this(new SimpleBoardGenerator(0));
  }

  public Grid(int[] board) {
    this(new ExternalBoardGenerator(board));
  }
  /**
   * @param value is an int to fill into board
   * @param row is a posstion where to fill(1-9).
   * @param column is a posstion where to fill(1-9).
   * @return true if it could fill the board or false if it couldn't
   */
  public boolean fill(int value, int row, int column) {
    Cell cell = getCell(row, column);
    if (cell == null || !cell.isModifiable()) {
      return false;
    }
    cell.setValue(value);
    return true;
  }

  /**
   * @param row is the a number between 1 and 9.
   * @param column is the a number between 1 and 9
   * @return true if it could clear the cell in a possition or false if it couldn't
   */
  public boolean clear(int row, int column) {
    Cell cell = getCell(row, column);
    if (cell == null || !cell.isModifiable()) {
      return false;
    }
    cell.clear();
    return true;
  }

  /**
   * @param row is the a number between 1 and 9.
   * @param column is the a number between 1 and 9
   * @return sudoku value on row and column position
   */
  public int getValue(int row, int column) {
    Cell cell = getCell(row, column);
    if (cell == null) {
      return OUT_OF_RANGE;
    }
    return cell.getValue();
  }


  // TODO: move it to an specific interface.
  public int rowSize() {
    return SIZE;
  }

  public int columnSize() {
    return SIZE;
  }

  public int blockSize() {
    return BLOCK_N;
  }

  /**
   * @param row int number between 0-8.
   * @return int[] row
   */
  public int[] getRow(int row) {
    int[] auxRow = new int[SIZE];
    for (int i = 0; i < SIZE; i++) {
      auxRow[i] = board[row][i].getValue();
    }
    return auxRow;
  }

  /**
   * @param column int number between 0-8.
   * @return int[] column
   */
  public int[] getColumn(int column) {
    int[] auxColumn = new int[SIZE];
    for (int i = 0; i < SIZE; i++) {
      auxColumn[i] = board[i][column].getValue();
    }
    return auxColumn;
  }

  /**
   * @param block value is between 1-9.
   * @return int[][] with the block values
   */
  public int[][] getBlock(int block) {
    int[][] auxBlock = new int[BLOCK_N][BLOCK_N];
    int[] entry = blockEntryPoint(block);
    int row = entry[0];
    int column = entry[1];
    for (int i = row; i < row + BLOCK_N; i++) {
      for (int j = column; j < column + BLOCK_N; j++) {
        auxBlock[i - row][j - column] = board[i][j].getValue();
      }
    }
    return auxBlock;
  }

  /**
   * @param row int value is between 1-9.
   * @param column int value is between 1-9.
   * @return int[][] with the block values
   */
  public int[][] getBlock(int row, int column) {
    int block = blockNumber(row, column);
    return getBlock(block);
  }

  @Override
  public int[] getGrid() {
    int position = 0;
    int[] auxBoard = new int[SIZE * SIZE];
    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
        auxBoard[position] = board[i][j].getValue();
        position++;
      }
    }
    return auxBoard;
  }

  @Override
  public String toString() {
    String rep = "";
    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
        rep += board[i][j].getValue() + " ";
      }
      rep += "\n";
    }
    return rep;
  }

  private Cell getCell(int row, int column) {
    if ((row < 0 || row > 8) || (column < 0 || column > 8)) {
      return null;
    }
    return this.board[row][column];
  }

  // Given a row and column it determine the block number that these values
  // belongs to. 1-9
  private int blockNumber(int row, int column) {
    int offset = 1;
    int with = 3;
    return ((row) / with * with) + column / 3 + offset;
  }

  // returns the correct start i, j numbers for a given block(1-9)
  private int[] blockEntryPoint(int block) {
    List<int[]> startPoints = new ArrayList<int[]>(SIZE);
    startPoints.add(new int[] {0, 0});
    startPoints.add(new int[] {0, 3});
    startPoints.add(new int[] {0, 6});
    startPoints.add(new int[] {3, 0});
    startPoints.add(new int[] {3, 3});
    startPoints.add(new int[] {3, 6});
    startPoints.add(new int[] {6, 0});
    startPoints.add(new int[] {6, 3});
    startPoints.add(new int[] {6, 6});
    return startPoints.get(block - 1);
  }

}
