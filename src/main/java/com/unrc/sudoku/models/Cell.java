package com.unrc.sudoku.models;

import java.lang.IllegalArgumentException;
import java.lang.IllegalStateException;

public class Cell {

  private int value;
  private boolean modifiable;

  public Cell(int value) {
    this.value = value;
    this.modifiable = false;
  }

  public Cell() {
    this.value = 0;
    this.modifiable = true;
  }

  public boolean isModifiable() {
    return this.modifiable;
  }

  public int getValue() {
    return this.value;
  }

  /**
   * @param value is an int between 1 and 9
   * @exception IllegalStateException if the cell is not modifiable
   * @exception IllegalArgumentException if value does not in [1..9]
   */
  public void setValue(int value) {
    if (!isModifiable()) {
      throw new IllegalStateException("The cell is not modifiable");
    }
    if (value < 1 || value > 9) {
      throw new IllegalArgumentException("The value must be between 1 and 9");
    }
    this.value = value;
  }

  /**
   * @exception IllegalStateException if the cell is not modifiable.
   */
  public void clear() {
    if (!isModifiable()) {
      throw new IllegalStateException("The cell is not modifiable");
    }
    this.value = 0;
  }

  @Override
  public String toString() {
    return "<Cell value: " + String.valueOf(this.value)
           + ", modifiable: " + String.valueOf(this.modifiable)
           + "\n";
  }
}
