package com.unrc.sudoku.models;

public class SolveChecker {
  private Grid grid;

  public SolveChecker(Grid grid) {
    this.grid = grid;
  }

  /**
   * @param value this is the number to check.
   * @param row this is the number of row on sudoku(1-9)
   * @return true if value in that row is valid or false if not
   */
  public boolean rowValid(int value, int row) {
    int match = 0;
    int[] auxRow = grid.getRow(row);

    for (int i = 0; i < grid.rowSize(); i++) {
      if (auxRow[i] == value) {
        match++;
      }
    }
    return match <= 1;
  }

  /**
   * @param value this is the number to check.
   * @param column this is the number of column on sudoku(1-9)
   * @return true if value in that column is valid or false if not
   */
  public boolean columnValid(int value, int column) {
    int match = 0;
    int[] auxColumn = grid.getColumn(column);
    for (int i = 0; i < grid.columnSize(); i++) {
      if (auxColumn[i] == value) {
        match++;
      }
    }
    return match <= 1;
  }

  /**
   * @param value this is the number to check.
   * @param row this is the number of row on sudoku(1-9)
   * @param column this is the number of row on sudoku(1-9)
   * @return true if value in the a block(row, column) is valid or false if not
   */
  public boolean blockValid(int value, int row, int column) {
    int match = 0;
    int[][] block = grid.getBlock(row, column);
    for (int i = 0; i < grid.blockSize(); i++) {
      for (int j = 0; j < grid.blockSize(); j++) {
        if (block[i][j] == value) {
          match++;
        }
      }
    }
    return match <= 1;
  }
}
