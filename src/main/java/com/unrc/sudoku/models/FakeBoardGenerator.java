package com.unrc.sudoku.models;

// Solved
//     0  1  2  3  4  5  6  7  8
// A { 4, 3, 5, 2, 6, 9, 7, 8, 1 },
// B { 6, 8, 2, 5, 7, 1, 4, 9, 3 },
// C { 1, 9, 7, 8, 3, 4, 5, 6, 2 },
// D { 8, 2, 6, 1, 9, 5, 3, 4, 7 },
// E { 3, 7, 4, 6, 8, 2, 9, 1, 5 },
// F { 9, 5, 1, 7, 4, 3, 6, 2, 8 },
// G { 5, 1, 9, 3, 2, 6, 8, 7, 4 },
// H { 2, 4, 8, 9, 5, 7, 1, 3, 6 },
// I { 7, 6, 3, 4, 1, 8, 2, 5, 9 }

public class FakeBoardGenerator implements BoardGenerator {
  static final int SIZE = 9;
  static final int EMPTY = 0;
  private boolean solved = false;

  public FakeBoardGenerator() {
  }

  public FakeBoardGenerator(boolean solved) {
    this.solved = solved;
  }


  public Cell[][] run() {
    Cell[][] board = new Cell[SIZE][SIZE];

    int[][] values = solved ? getSolved() : getNotSoved();

    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
        int value = values[i][j];
        board[i][j] = value == EMPTY ? new Cell() : new Cell(value);
      }
    }
    return board;
  }

  private int[][] getSolved() {
    int[][] board = {
      { 4, 3, 5, 2, 6, 9, 7, 8, 1 },
      { 6, 8, 2, 5, 7, 1, 4, 9, 3 },
      { 1, 9, 7, 8, 3, 4, 5, 6, 2 },
      { 8, 2, 6, 1, 9, 5, 3, 4, 7 },
      { 3, 7, 4, 6, 8, 2, 9, 1, 5 },
      { 9, 5, 1, 7, 4, 3, 6, 2, 8 },
      { 5, 1, 9, 3, 2, 6, 8, 7, 4 },
      { 2, 4, 8, 9, 5, 7, 1, 3, 6 },
      { 7, 6, 3, 4, 1, 8, 2, 5, 9 }
    };
    return board;
  }

  private int[][] getNotSoved() {
    int[][]board = {
      { 0, 0, 0, 2, 6, 0, 7, 0, 1 },
      { 6, 8, 0, 0, 7, 0, 0, 9, 0 },
      { 1, 9, 0, 0, 0, 4, 5, 0, 0 },
      { 8, 2, 0, 1, 0, 0, 0, 4, 0 },
      { 0, 0, 4, 6, 0, 2, 9, 0, 0 },
      { 0, 5, 0, 0, 0, 3, 0, 2, 8 },
      { 0, 0, 9, 3, 0, 0, 0, 7, 4 },
      { 0, 4, 0, 0, 5, 0, 0, 3, 6 },
      { 7, 0, 3, 0, 1, 8, 0, 0, 0 }
    };
    return board;
  }
}