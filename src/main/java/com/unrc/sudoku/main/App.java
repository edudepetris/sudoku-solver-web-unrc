package com.unrc.sudoku.main;

import static spark.Spark.*;
import com.unrc.sudoku.router.Path;
import com.unrc.sudoku.controllers.SudokuController;

public class App {

  public static void main( String[] args ) {

    // Enables CORS on requests.
    enableCORS("*", "*", "*");

    // Set up routes
    get(Path.Sudoku.NEW, SudokuController.fecthNewSudoku);
    post(Path.Sudoku.CHECK, SudokuController.checkSudoku);
    notFound("<html><body><h1>Custom 404 handling</h1></body></html>");
  }

  // This method is an initialization method and should be called once.
  private static void enableCORS(final String origin, final String methods, final String headers) {

    options("/*", (request, response) -> {
      /**/
      String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
      if (accessControlRequestHeaders != null) {
        response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
      }

      String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
      if (accessControlRequestMethod != null) {
        response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
      }

      return "OK";
    });

    before((request, response) -> {
      response.header("Access-Control-Allow-Origin", origin);
      response.header("Access-Control-Request-Method", methods);
      response.header("Access-Control-Allow-Headers", headers);
      // Note: this may or may not be necessary in your particular application
      response.type("application/json");
    });
  }
}

