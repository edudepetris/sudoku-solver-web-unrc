package com.unrc.sudoku.router;

// It’s usually a good idea to keep your paths in some sort of constant.
// Ex. Path class with a static nested class Web (it also has a static nested
//  class Template), which holds public final static Strings. That’s just
// my preference, it’s up to you how you want to do this.
// All my handlers are declared as static Route fields, grouping together
// functionality in the same classes (based on feature).
public class Path {

  public static class Sudoku {
    public static final String NEW = "/sudoku/new";
    public static final String CHECK = "/sudoku/check";
  }
}