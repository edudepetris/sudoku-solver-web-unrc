# Sudoku

### Java implementation for training on DOSE.

### Develop
Compile and run test with the following command:

```shell
# compile and run
$ mvn compile exec:java

# compile and run test
$ mvn test
```

### Checkstyle
```shell
$ mvn site
$ mvn checkstyle:checkstyle
```

### References
- [Checkstyle](https://maven.apache.org/plugins/maven-checkstyle-plugin/usage.html)
- [Sudoku](https://en.wikipedia.org/wiki/Sudoku)
- [Junit](https://github.com/junit-team/junit4/wiki/Getting-started)
- [Generate puzzles](http://dryicons.com/blog/2009/08/14/a-simple-algorithm-for-generating-sudoku-puzzles/)
